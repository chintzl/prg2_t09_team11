﻿//============================================================
// Student Number : S10208283, S10208484
// Student Name : To Chin Liang, Irene Phang Xin Ping
// Module Group : T09
//============================================================
using System;
using System.Collections.Generic;
using System.Text;

namespace COVID_19_Monitoring_System
{
    abstract class Person
    {
        //Attributes
        public string Name { get; set; }
        public bool IsInfected { get; set; }
        public List<SafeEntry> SafeEntryList = new List<SafeEntry>();
        public List<TravelEntry> TravelEntryList = new List<TravelEntry>();
        //End of Attributes

        //Constructors
        public Person() 
        { 
            List<SafeEntry> SafeEntryList = new List<SafeEntry>();
            List<TravelEntry> TravelEntryList = new List<TravelEntry>();
        }

        public Person(string n)
        {
            Name = n;
            List<SafeEntry> SafeEntryList = new List<SafeEntry>();
            List<TravelEntry> TravelEntryList = new List<TravelEntry>();
        }
        //End of Constructors

        //Methods
        public void AddTravelEntry(TravelEntry te)
        {
            TravelEntryList.Add(te);
        }
        public void AddSafeEntry(SafeEntry se)
        {
            SafeEntryList.Add(se);
        }

        public abstract double CalculateSHNCharges();
        public override string ToString()
        {
            string str = "Name: " + Name + "\n";
            foreach(SafeEntry se in SafeEntryList)
            {
                str += se.ToString();
            }
            foreach(TravelEntry te in TravelEntryList)
            {
                str += te.ToString();
            }
            return str;
        }
        //End of Methods
    }//End of Person class
}//End of namespace

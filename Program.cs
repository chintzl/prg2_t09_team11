﻿//============================================================
// Student Number : S10208283, S10208484
// Student Name : To Chin Liang, Irene Phang Xin Ping
// Module Group : T09
//============================================================
using System;
using System.IO;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json;

namespace COVID_19_Monitoring_System
{
    class Program
    {
        static void Main(string[] args)
        {
            List<SHNFacility> shnFacilityList = LoadSHNFacilityData(); //Creating SHNFacility list and calling LoadSHNFacilityData method to load SHNFacility object into it
            List<Person> personList = new List<Person>(); //Creating Person list
            List<BusinessLocation> bizLocationList = new List<BusinessLocation>(); //Creating BusinessLocation list
            LoadBizLocation(bizLocationList); //Calling LoadBizLocation method
            LoadPerson(personList, shnFacilityList); //Calling LoadPerson method
            bool checkMain = true;
            bool checkGeneral;
            bool checkTrace;
            bool checkTravel;
            bool checkReport;
            int option;
            while (checkMain == true)
            {
                try
                {
                    DisplayMainMenu();
                    Console.Write("Please enter your option: ");
                    option = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine();
                    if (option == 1) //General menu
                    {
                        checkGeneral = true;
                        while (checkGeneral == true)
                        {
                            DisplayGeneralMenu();
                            Console.Write("Please enter your option: ");
                            option = Convert.ToInt32(Console.ReadLine());
                            Console.WriteLine();
                            if (option == 1) //list all visitors
                            {
                                DisplayVisitor(personList); //calling DisplayVisitor method
                            }
                            else if (option == 2) //list person
                            {
                                ListPersonDetails(personList); //calling ListPersonDetails method
                            }
                            else if (option == 0) //Exit to Main Menu
                            {
                                checkGeneral = false;
                            }
                            else
                            {
                                Console.WriteLine("\nPlease enter a correct option!");
                            }
                        }
                    }
                    else if (option == 2) //Trace Together Menu
                    {
                        checkTrace = true;
                        while (checkTrace == true)
                        {
                            DisplayTraceTogetherMenu();
                            Console.Write("Please enter your option: ");
                            option = Convert.ToInt32(Console.ReadLine());
                            Console.WriteLine();
                            if (option == 1) //Assign/Replace TraceTogether Token
                            {
                                AssignReplaceTraceTogetherToken(personList); //calling AssignReplaceTraceTogetherToken method
                            }
                            else if (option == 2) //List all Business Location
                            {
                                DisplayBizLocation(bizLocationList); //calling DisplayBizLocation method
                            }
                            else if (option == 3) //Edit Business Location Capacity
                            {
                                EditBizCap(bizLocationList); //calling EditBizCap method
                            }
                            else if (option == 4) //SafeEntry Check In
                            {
                                SafeEntryCheckIn(personList, bizLocationList); //calling SafeEntryCheckIn method
                            }
                            else if (option == 5) //SafeEntry Check Out
                            {
                                SafeEntryCheckOut(personList, bizLocationList); //calling SafeEntryCheckOut method
                            }
                            else if (option == 0) //Exit to Main Menu
                            {
                                checkTrace = false;
                            }
                            else
                            {
                                Console.WriteLine("\nPlease enter a correct option!");
                            }
                        }
                    }
                    else if (option == 3) //Travel Entry Menu
                    {
                        checkTravel = true;
                        while (checkTravel == true)
                        {
                            DisplayTravelEntryMenu();
                            Console.Write("Please enter your option: ");
                            option = Convert.ToInt32(Console.ReadLine());
                            Console.WriteLine();
                            if (option == 1) //list all SHN Facilities
                            {
                                DisplaySHNFacility(shnFacilityList); //calling DisplaySHNFacility method
                            }
                            else if (option == 2) //create Visitor
                            {
                                CreateVisitor(personList); //calling CreateVisitor method
                            }
                            else if (option == 3) //create TravelEntry Record
                            {
                                CreateTravelEntryRecord(personList, shnFacilityList); //calling CreateTravelEntryRecord method
                            }
                            else if (option == 4) //calculate SHN Charges
                            {
                                CalculcateNotPaidSHNCharges(personList); //calling CalculateNotPaidSHNCharges method
                            }
                            else if (option == 0) //Exit to Main Menu
                            {
                                checkTravel = false;
                            }
                            else
                            {
                                Console.WriteLine("\nPlease enter a correct option!");
                            }
                        }
                    }
                    else if (option == 4) //Advanced menu
                    {
                        checkReport = true;
                        while (checkReport == true)
                        {
                            DisplayReportMenu();
                            Console.Write("Please enter your option: ");
                            option = Convert.ToInt32(Console.ReadLine());
                            Console.WriteLine();
                            if (option == 1) //Contact Tracing Reporting
                            {
                                GenerateContactTracingReport(personList); //calling GenerateContactTracingReport method
                            }
                            else if (option == 2) //SHN Status Reporting
                            {
                                GenerateSHNStatusReport(personList, shnFacilityList); //calling GenerateSHNStatusReport method
                            }
                            else if (option == 3) //Infected Person 
                            {
                                InfectedPerson(personList, bizLocationList); //calling InfectedPerson method
                            }
                            else if (option == 4) //Infected Person Reporting
                            {
                                GenerateInfectedPersonReport(personList); //calling GenerateInfectedPersonReport method
                            }
                            else if (option == 0)
                            {
                                checkReport = false;
                            }
                        }
                    }
                    else if (option == 0) //Exit Program
                    {
                        checkMain = false;
                    }
                    else
                    {
                        Console.WriteLine("Please enter a correct option!\n");
                    }
                }
                catch (FormatException)
                {
                    Console.WriteLine("\nPlease enter a correct option!\n");
                }
                catch (Exception)
                {
                    Console.WriteLine("\nPlease enter a correct option!\n");
                }
            }
        }
        //DisplayMainMenu method to display Main Menu
        static void DisplayMainMenu()
        {
            Console.WriteLine("-------Main Menu-------");
            Console.WriteLine("[1] General Menu");
            Console.WriteLine("[2] Trace Together Menu");
            Console.WriteLine("[3] Travel Entry Menu");
            Console.WriteLine("[4] Report Menu");
            Console.WriteLine("[0] Exit");
            Console.WriteLine("-----------------------");
        }
        //DisplayGeneralMenu method to display General Menu
        static void DisplayGeneralMenu()
        {
            Console.WriteLine("-------General Menu-------");
            Console.WriteLine("[1] List all Visitors");
            Console.WriteLine("[2] List Person Details");
            Console.WriteLine("[0] Back to Main Menu");
            Console.WriteLine("--------------------------");
        }
        //DisplayTraceTogetherMenu method to display Trace Together Menu
        static void DisplayTraceTogetherMenu()
        {
            Console.WriteLine("----------Trace Together Menu----------");
            Console.WriteLine("[1] Assign/Replace TraceTogether Token");
            Console.WriteLine("[2] List all Business Location");
            Console.WriteLine("[3] Edit Business Location Capacity");
            Console.WriteLine("[4] SafeEntry Check In");
            Console.WriteLine("[5] SafeEntry Check Out");
            Console.WriteLine("[0] Back to Main Menu");
            Console.WriteLine("---------------------------------------");
        }
        //DisplayTravelEntryMenu method to display Travel Entry Menu
        static void DisplayTravelEntryMenu()
        {
            Console.WriteLine("------Travel Entry Menu------");
            Console.WriteLine("[1] List all SHN Facilities");
            Console.WriteLine("[2] Create Visitor");
            Console.WriteLine("[3] Create TravelEntry Record");
            Console.WriteLine("[4] Calculate SHN Charges");
            Console.WriteLine("[0] Back to Main Menu");
            Console.WriteLine("-----------------------------");
        }
        //DisplayReportMenu method to display report menu 
        static void DisplayReportMenu()
        {
            Console.WriteLine("----------Report Menu---------");
            Console.WriteLine("[1] Contact Tracing Reporting");
            Console.WriteLine("[2] SHN Status Reporting");
            Console.WriteLine("[3] Infected People");
            Console.WriteLine("[4] Infected People Reporting");
            Console.WriteLine("[0] Back to Main Menu");
            Console.WriteLine("-----------------------------");
        }

        //LoadBizLocation method to load all Business Locations
        static void LoadBizLocation(List<BusinessLocation> bList)
        {
            string[] stringArray = File.ReadAllLines("BusinessLocation.csv"); //reading data from file

            for (int i = 1; i < stringArray.Length; i++)
            {
                string[] dataArray = stringArray[i].Split(",");
                string name = dataArray[0];
                string loc = dataArray[1];
                int max = Convert.ToInt32(dataArray[2]);
                BusinessLocation b = new BusinessLocation(name, loc, max); //assigning new BusinessLocation object
                bList.Add(b); //adding BusinessLocation object into a list
            }
        }
        //LoadPerson method to load csv data into Person list
        static void LoadPerson(List<Person> pList, List<SHNFacility> sList)
        {
            string[] stringArray = File.ReadAllLines("Person.csv"); //reading data from file
            for (int i = 1; i < stringArray.Length; i++)
            {
                string[] dataArray = stringArray[i].Split(",");
                if (dataArray[0] == "resident") //check for resident
                {
                    Resident r = new Resident(dataArray[1], dataArray[2], DateTime.ParseExact(dataArray[3], "dd/MM/yyyy", null)); //assigning new Resident object
                    if (!(String.IsNullOrEmpty(dataArray[6]))) //check for null or empty string
                    {
                        r.Token = new TraceTogetherToken(dataArray[6], dataArray[7], DateTime.ParseExact(dataArray[8], "dd-MMM-yy", null)); //assigning new TraceTogetherToken object
                    }
                    if (!(String.IsNullOrEmpty(dataArray[9]))) //check for null or empty string
                    {
                        TravelEntry te = new TravelEntry(dataArray[9], dataArray[10], DateTime.ParseExact(dataArray[11], "dd/MM/yyyy H:mm", null)); //assigning new TravelEntry object
                        te.ShnEndDate = DateTime.ParseExact(dataArray[12], "dd/MM/yyyy H:mm", null);
                        if (!(String.IsNullOrEmpty(dataArray[13]))) //check for null or empty string
                        {
                            te.IsPaid = Convert.ToBoolean(dataArray[13]);
                            r.AddTravelEntry(te); //adding TravelEntry object Resident object
                        }
                        if (!(String.IsNullOrEmpty(dataArray[14]))) //check for null or empty string
                        {
                            te.AssignSHNFacility(SearchFacility(sList, dataArray[14])); //calling for SearchFacility method to search for facility and using AssignSHNFacility method to assign facility
                        }
                    }
                    pList.Add(r); //adding Resident object into a list
                }
                else if (dataArray[0] == "visitor") //check for visitor
                {
                    Visitor v = new Visitor(dataArray[1], dataArray[4], dataArray[5]); //assigning new Visitor object
                    if (!(String.IsNullOrEmpty(dataArray[9]))) //check for null or empty string
                    {
                        TravelEntry te = new TravelEntry(dataArray[9], dataArray[10], DateTime.ParseExact(dataArray[11], "dd/MM/yyyy H:mm", null)); //assigning new TravelEntry object
                        te.ShnEndDate = DateTime.ParseExact(dataArray[12], "dd/MM/yyyy H:mm", null);
                        if (!(String.IsNullOrEmpty(dataArray[13])))//check for null or empty string
                        {
                            te.IsPaid = Convert.ToBoolean(dataArray[13]);
                            v.AddTravelEntry(te); //adding TravelEntry object to Visitor object
                        }
                        if (!(String.IsNullOrEmpty(dataArray[14]))) //check for null or empty string
                        {
                            te.AssignSHNFacility(SearchFacility(sList, dataArray[14])); //calling for SearchFacility method to search for facility and using AssignSHNFacility method to assign facility
                        }
                    }
                    pList.Add(v); //adding Visitor object into a list
                }
            }
        }
        //SearchFacility method to search for the string Facility Name in correspond to the SHNFacilityList Name
        static SHNFacility SearchFacility(List<SHNFacility> sList, string c)
        {
            foreach (SHNFacility s in sList)
            {
                if (c.ToUpper() == s.FacilityName.ToUpper()) //check for matching SHNFacility object name to the given string name
                {
                    return s; //returning the SHNFacility object
                }
            }
            return null;
        }
        //DisplayBizLocation method to display BusinessLocation objects in businessList
        static void DisplayBizLocation(List<BusinessLocation> bList)
        {
            Console.WriteLine("{0,-20}  {1,-20}  {2,-20}", "Business Name", "Branch Code", "Maximum Capacity");
            foreach (BusinessLocation bl in bList)
            {
                Console.WriteLine("{0,-20}  {1,-20}  {2,-20}", bl.BusinessName, bl.BranchCode, bl.MaximumCapacity);
            }
            Console.WriteLine();
        }
        //ListPersonDetails method to prompt user for a name and return the Person object
        static void ListPersonDetails(List<Person> pList)
        {
            try
            {
                Console.Write("Please enter your name: "); //prompt user for input
                string name = Console.ReadLine(); 
                Person p = SearchPerson(pList, name); //calling SearchPerson method and assigning new Person object
                if (p != null) //check if the person object is not null
                {
                    Console.WriteLine(p.ToString()); //calling ToString method
                    if (p is Resident) //check if the Person object is a Resident object
                    {
                        Resident r = (Resident)p; //down-casting the Person object into a Resident object
                        if (r.Token.SerialNo != null) //check if the Resident object have a TraceTogetherToken
                        {
                            Console.WriteLine("\nTrace Together Token" + "\n---------------------");
                            Console.WriteLine(r.Token.ToString());
                        }
                    }
                    Console.WriteLine();
                }
                else 
                {
                    Console.WriteLine("No such person exist!\n");
                }
            }
            catch (FormatException)
            {
                Console.WriteLine("Please enter a valid value!\n");
            }
            catch (Exception)
            {
                Console.WriteLine("Please enter a valid value!\n");
            }

        }
        //DisplayVisitor method to display all Visitor object in Person list
        static void DisplayVisitor(List<Person> pList)
        {
            foreach (Person p in pList) 
            {
                if (p is Visitor) //check if the Person object is a Visitor object
                {
                    Console.WriteLine();
                    Visitor v = (Visitor)p; //down-casting the Person object into a Visitor object
                    Console.WriteLine(v.ToString()); //calling ToString method
                }
            }
            Console.WriteLine();
        }
        //SearchPerson method to search for a Person object given the name
        static Person SearchPerson(List<Person> pList, string name)
        {
            foreach (Person p in pList)
            {
                if (name.ToUpper() == p.Name.ToUpper()) //check for matching Person object name with given string name
                {
                    return p; //return Person object
                }
            }
            return null;
        }
        //LoadSHNFacilityData method to load SHNFacility list with SHNFacility objects from Web API
        static List<SHNFacility> LoadSHNFacilityData()
        {
            List<SHNFacility> sList = new List<SHNFacility>(); //create new SHNFacility List
            using (HttpClient client = new HttpClient()) //using web api to read data
            {
                client.BaseAddress = new Uri("https://covidmonitoringapiprg2.azurewebsites.net");
                Task<HttpResponseMessage> responseTask = client.GetAsync("/facility");
                responseTask.Wait();
                HttpResponseMessage result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    Task<string> readTask = result.Content.ReadAsStringAsync();
                    readTask.Wait();
                    string data = readTask.Result;
                    sList = JsonConvert.DeserializeObject<List<SHNFacility>>(data);
                    foreach(SHNFacility sf in sList) 
                    {
                        sf.FacilityVacancy = sf.FacilityCapacity;
                    }
                }
                return sList; //return SHNFaclity list
            }
        }
        //DisplaySHNFacility method to display all SHNFacility objects 
        static void DisplaySHNFacility(List<SHNFacility> sList)
        {
            foreach (SHNFacility s in sList)
            {
                Console.WriteLine(s.ToString()); //calling ToString mathod
                Console.WriteLine();
            }
        }
        //GenerateSerialNo method to generate random serial number for token
        static string GenerateSerialNo(List<Person> pList)
        {
            Random randNum = new Random(); //calling for Random class
            string serialNo = "T" + Convert.ToString(randNum.Next(10000, 99999)); //assigning random serial number to string
            foreach (Person p in pList)
            {
                if (p is Resident) //check if Person object is a Resident object
                {
                    Resident r = (Resident)p; //down-casting Person object into a Resident object
                    if (!(String.IsNullOrEmpty(r.Token.SerialNo))) //check if Resident object has a TraceTogetherToken
                    {
                        if (r.Token.SerialNo == serialNo) //check for similar serial number that is assigned
                        {
                            GenerateSerialNo(pList); //calling GenerateSerialNo method
                        }
                    }
                }
            }
            return serialNo; //return serialNo
        }
        //AssignReplaceTraceTogetherToken method to Assign/Replace TraceTogether Token
        static void AssignReplaceTraceTogetherToken(List<Person> pList)
        {
            try
            {
                Console.Write("Please enter your name: "); //prompt user for input
                string name = Console.ReadLine();
                Person p = SearchPerson(pList, name); //calling SearchPerson method
                if (p != null) //check for Person object is not null
                {
                    if (p is Resident) //check if Person object is a Resident object
                    {
                        Resident r = (Resident)p; //down-casting Person object into a Resident object
                        if ((String.IsNullOrEmpty(r.Token.SerialNo))) //check if there is a token assigned to Resident object
                        {
                            string serialNo = GenerateSerialNo(pList); //calling GenerateSerialNo method
                            Console.Write("Please enter your Collection Location: "); //prompt user for input
                            string collectionLocation = Console.ReadLine();
                            DateTime expiryDate = DateTime.Now.AddMonths(6); 
                            r.Token = new TraceTogetherToken(serialNo, collectionLocation, expiryDate); //create new TraceTogetherToken and assign to Resident object
                            Console.WriteLine("Trace Together Token has been assign successfully!\n");
                        }
                        else //a token is assigned
                        {
                            if (r.Token.IsEligibleForReplacement() == true) //check if the token is eligible for replacement
                            {
                                string serialNo = GenerateSerialNo(pList); ///calling GenerateSerialNo method
                                Console.Write("Please enter your Collection Location: "); //prompt user for input
                                string collectionLocation = Console.ReadLine();
                                DateTime expiryDate = DateTime.Now.AddMonths(6);
                                r.Token = new TraceTogetherToken(serialNo, collectionLocation, expiryDate);//create new TraceTogetherToken and assign to Resident object
                                Console.WriteLine("Trace Together Token has been replaced successfully!\n");
                            } 
                            else //token is not eligible for replacement
                            {
                                Console.WriteLine("Your token is not eligible for replacement!\n");
                            }
                        }
                    }
                    else
                    {
                        Console.WriteLine("You are not a resident!\n");
                    }
                }
                else
                {
                    Console.WriteLine("No such person exist!\n");
                }
            }
            catch (FormatException)
            {
                Console.WriteLine("Please enter a valid value!\n");
            }
            catch (Exception)
            {
                Console.WriteLine("Please enter a valid value!\n");
            }
            
        }
        //SearchBusinesLocation method to search for Business Location object given the name
        static BusinessLocation SearchBusinessLocation(List<BusinessLocation> bList, string bName)
        {
            foreach(BusinessLocation b in bList)
            {
                if (b.BusinessName.ToUpper() == bName.ToUpper()) //check for matching BusinessLocation object name with given string name
                {
                    return b; //return BusinessLocation object
                }
            }
            return null;
        }
        //Edit Business Location Capacity
        static void EditBizCap(List<BusinessLocation> bList)
        {
            try
            {
                Console.Write("Please enter Business Name: "); //prompt user for input
                string bName = Console.ReadLine();
                BusinessLocation b = SearchBusinessLocation(bList, bName); //calling SearchBusinessLocation method
                if (b != null) //check if the Business Location object is not null
                {
                    Console.Write("Edit the maximum capacity: "); //prompt user for input
                    b.MaximumCapacity = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine("Maximum capacity for {0} has been updatead successfully!\n",b.BusinessName);
                }
                else
                {
                    Console.WriteLine("No such Business exist!\n");
                }
            }
            catch (FormatException)
            {
                Console.WriteLine("Please enter a valid value!\n");
            }
            catch (Exception)
            {
                Console.WriteLine("Please enter a valid value!\n");
            }
        }
        //SafeEntry Check-in
        static void SafeEntryCheckIn(List<Person> pList, List<BusinessLocation> bList)
        {
            try
            {
                Console.Write("Please enter your name: "); //prompt user for input
                string name = Console.ReadLine();
                Person p = SearchPerson(pList, name); //calling SearchPerson method 
                if (p != null) //check for Person object is not null
                {
                    Console.WriteLine();
                    DisplayBizLocation(bList); //calling DisplayBizLocation method
                    Console.Write("Enter your Business Location Name to Check In: "); //prompt user for input
                    string bName = Console.ReadLine();
                    BusinessLocation b = SearchBusinessLocation(bList, bName); //calling SearchBusinessLocation method
                    if (b != null) //check for the BusinessLocation object is not null
                    {
                        if (b.IsFull() != true) //check for the business is not full
                        {
                            SafeEntry safeIn = new SafeEntry(DateTime.Now, b); //create new SafeEntry object
                            b.VisitorsNow += 1; //increment of VisitorsNow in BusinessLocation object
                            p.AddSafeEntry(safeIn); //calling AddSafeEntry method
                            Console.WriteLine("You have Check In into {0}!\n", b.BusinessName);
                        }
                        else
                        {
                            Console.WriteLine("Business Location {0} is now full!\n", b.BusinessName);
                        }
                    }
                    else
                    {
                        Console.WriteLine("No such Business exist!\n");
                    }
                }
                else
                {
                    Console.WriteLine("No such Person exist!\n");
                }
            }
            catch (FormatException)
            {
                Console.WriteLine("Please enter a valid value!\n");
            }
            catch (Exception)
            {
                Console.WriteLine("Please enter a valid value!\n");
            }
        }
        //SafeEntry Check-out
        static void SafeEntryCheckOut(List<Person> pList, List<BusinessLocation> bList)
        {
            try
            {
                bool checkEmpty = false;
                Console.Write("Please enter your Name: "); //prompt user for input
                string name = Console.ReadLine();
                Person p = SearchPerson(pList, name); //calling SearchPerson method
                if (p != null) //check for Person object is not null
                {
                    if (p.SafeEntryList.Count != 0)
                    {
                        foreach (SafeEntry se in p.SafeEntryList)
                        {
                            if (se.CheckOut == default(DateTime))
                            {
                                Console.WriteLine(se.ToString());
                            }
                            else
                            {
                                Console.WriteLine("No Business Location to Check Out from!\n");
                                checkEmpty = true;
                            }
                        }
                        if (checkEmpty == false)
                        {
                            Console.Write("Please enter Business Location Name to Check Out: "); //prompt user for input
                            string bName = Console.ReadLine();
                            BusinessLocation b = SearchBusinessLocation(bList, bName); //calling SearchBusinessLocation method
                            if (b != null) //check for the BusinessLocation object is not null
                            {
                                foreach (SafeEntry se in p.SafeEntryList)
                                {
                                    if (se.Location.BusinessName == b.BusinessName)
                                    {
                                        se.PerformCheckOut(); //calling PerformCheckOut method
                                        b.VisitorsNow -= 1; //decrement of VisitorsNow in BusinessLocation object
                                        break;
                                    }
                                }
                                Console.WriteLine("You have Check Out from {0}!\n", b.BusinessName);
                            }
                            else
                            {
                                Console.WriteLine("No such Business exist!\n");
                            }
                        }
                    }
                    else
                    {
                        Console.WriteLine("No Business Location to Check Out!\n");
                    }
                }
                else
                {
                    Console.WriteLine("No such Person exist!\n");
                }
            }
            catch (FormatException)
            {
                Console.WriteLine("Please enter a valid value!\n");
            }
            catch (Exception)
            {
                Console.WriteLine("Please enter a valid value!\n");
            }
        }
        //CreateVisitor method to create a Visitor object and add into Person list
        static void CreateVisitor(List<Person> pList)
        {
            try
            {
                Console.Write("Please enter your Name: "); //prompt user for input
                string name = Console.ReadLine();
                Console.Write("Please enter your Passport Number: "); //prompt user for input
                string passNo = Console.ReadLine();
                Console.Write("Please enter your Nationality: "); //prompt user for input
                string na = Console.ReadLine();
                Visitor v = new Visitor(name, passNo, na); //create new Visitor object
                pList.Add(v); //adding Visitor object into a list
                Console.WriteLine("New Visitor has been added!\n");
            }
            catch (FormatException)
            {
                Console.WriteLine("Please enter a valid value!\n");
            }
            catch (Exception)
            {
                Console.WriteLine("Please enter a valid value!\n");
            }
        }
        //CreateTravelEntryRecord method to create a Travel Entry record with given name from user
        static void CreateTravelEntryRecord(List<Person> pList, List<SHNFacility> sList)
        {
            try
            {
                Console.Write("Please enter your Name: "); //prompt user for input
                string name = Console.ReadLine();
                Person p = SearchPerson(pList, name);
                if (p != null) //check for Person object is not null
                    {
                        Console.Write("Please enter Last Country of Embarkation: "); //prompt user for input
                        string last = Console.ReadLine();
                    if (last != "")
                    {
                        Console.Write("Please enter Entry Mode 'Sea','Air','Land': "); //prompt user for input
                        string em = Console.ReadLine();
                        if (em.ToUpper() == "SEA" || em.ToUpper() == "AIR" || em.ToUpper() == "LAND")
                        {
                            TravelEntry te = new TravelEntry(last, em, DateTime.Now); //create new TravelEntry object
                            te.CalculateSHNDuration(); //calling CalculateSHNDuration method
                            if (te.ShnEndDate == te.EntryDate) //No need to serve SHN
                            {
                                Console.WriteLine("You are not required to serve SHN!\n");
                            }
                            else if (te.ShnEndDate == te.EntryDate.AddDays(7)) //Need to serve 7 days SHN at home
                            {
                                Console.WriteLine("You are required to serve 7 days SHN at home!\n");
                            }
                            else if (te.ShnEndDate == te.EntryDate.AddDays(14)) //Need to serve 14 days SHN at facility
                            {
                                Console.WriteLine();
                                DisplaySHNFacility(sList); //calling DisplaySHNFacility method
                                Console.WriteLine("You are required to serve 14 days SHN at a facility!\n");
                                Console.Write("Enter name of SHN Facility to stay: "); //prompt user for input
                                string sName = Console.ReadLine();
                                SHNFacility s = SearchFacility(sList, sName); //calling SearchFacility method
                                if (s != null) //check for SHNFacility is not null
                                {
                                    if (s.IsAvailable() == true) //check if SHNFacility is available for booking
                                    {
                                        te.AssignSHNFacility(s); //calling AssignSHNFacility method
                                        p.AddTravelEntry(te); //calling AddTravelEntry method
                                        Console.WriteLine("New Travel Entry has been added!\n");
                                    }
                                    else
                                    {
                                        Console.WriteLine("Facility is full at the moment, please try again!\n");
                                    }
                                }
                                else
                                {
                                    Console.WriteLine("No such Facility Exist!\n");
                                }
                            }
                        }
                        else
                        {
                            Console.WriteLine("Please enter a valid Entry Mode!\n");
                        }
                    }
                    else
                    {
                        Console.WriteLine("Please enter a valid value!\n");
                    }
                        
                }
                else
                {
                    Console.WriteLine("No such Person exist!\n");
                }
            }
            catch (FormatException)
            {
                Console.WriteLine("Please enter a valid value!\n");
            }
            catch (Exception)
            {
                Console.WriteLine("Please enter a valid value!\n");
            }
        }
        //CalculateNotPaidSHNCharges method to calculate SHN charges that is not paid with given name from user
        static void CalculcateNotPaidSHNCharges(List<Person> pList)
        {
            try
            {
                Console.Write("Please enter your Name: "); //prompt user for input
                string name = Console.ReadLine();
                double amount = 0.0;
                Person p = SearchPerson(pList, name); //calling SearchPerson method
                if (p != null) //check for Person object is not null
                {
                    if (p.TravelEntryList != null)
                    {
                        foreach (TravelEntry entry in p.TravelEntryList)
                        {
                            if (entry.IsPaid == false) //check if SHNFacility payment has not been made
                            {
                                if (entry.ShnEndDate == entry.EntryDate || entry.ShnEndDate <= DateTime.Now) //check if SHN has ended
                                {
                                    amount = p.CalculateSHNCharges(); //calling CalculateSHNCharges method
                                    amount = amount * 1.07; //GST charge of 7%
                                    Console.WriteLine("Please make your payment of: ${0}", amount);
                                    Console.Write("Making payment now? (Y/N): ");
                                    string payment = Console.ReadLine();
                                    if (payment.ToUpper() == "Y")
                                    {
                                        entry.IsPaid = true;
                                        Console.WriteLine("Your payment of ${0} has been made, thank you!\n", amount);
                                    }
                                    else if (payment.ToUpper() == "N") 
                                    {
                                        Console.WriteLine("Please make your payment soon!\n");
                                    }
                                    else
                                    {
                                        Console.WriteLine("Please input Y/N only!\n");
                                    }
                                }
                            }
                            else
                            {
                                Console.WriteLine("No payment due!\n");
                            }
                        }
                    }
                    else
                    {
                        Console.WriteLine("No Travel Entry record exist!\n");
                    }
                }
                else
                {
                    Console.WriteLine("No such person exist!\n");
                }
            }
            catch (FormatException)
            {
                Console.WriteLine("Please enter a valid value!\n");
            }
            catch (Exception)
            {
                Console.WriteLine("Please enter a valid value!\n");
            }
        }
        //Advanced Features
        //GenerateContactTracingReport to generate a report of contact tracing
        static void GenerateContactTracingReport(List<Person> pList)
        {
            try
            {
                bool checkDone = false;
                Console.Write("Please enter a start date for the report (dd/MM/yyyy): ");
                DateTime startDate = Convert.ToDateTime(Console.ReadLine());
                Console.Write("Please enter a end date for the report (dd/MM/yyyy): ");
                DateTime endDate = Convert.ToDateTime(Console.ReadLine());
                Console.Write("Please enter the Business Name: ");
                string bName = Console.ReadLine();
                string heading = "Business Name" + "," + "Name" + "," + "Check In" + "," + "Check Out" + "\n";
                File.WriteAllText("ContactTracingReport.csv", heading);
                foreach (Person p in pList)
                {
                    foreach (SafeEntry se in p.SafeEntryList)
                    {
                        if (se.Location.BusinessName.ToUpper() == bName.ToUpper())
                        {
                            if (startDate <= se.CheckIn && endDate <= se.CheckOut)
                            {
                                string data = se.Location.BusinessName + "," + p.Name + "," + se.CheckIn + "," + se.CheckOut + "\n";
                                File.AppendAllText("ContactTracingReport.csv", data);
                                checkDone = true;
                            }
                        }
                        else if (se.Location.BusinessName.ToUpper() != bName.ToUpper())
                        {
                            Console.WriteLine("Business does not exist!\n");
                        }
                    }
                }
                if (checkDone == true)
                {
                    Console.WriteLine("Report generated successfully\n");
                }
                else
                {
                    Console.WriteLine("There is no person to generate the report\n");
                }
            }
            catch (FormatException)
            {
                Console.WriteLine("Please enter a valid value!\n");
            }
            catch (Exception)
            {
                Console.WriteLine("Please enter a valid value!\n");
            }
        }
        //GenerateSHNStatusReport method to generate a report of SHN Status
        static void GenerateSHNStatusReport(List<Person> pList,List<SHNFacility> sList)
        {
            try
            {
                bool checkDone = false;
                Console.Write("Please enter a start date for the report (dd/MM/yyyy): "); //prompt user for input
                DateTime startDate = Convert.ToDateTime(Console.ReadLine());
                string heading = "Report Date for: \t" + startDate + "\n" + "Name" + "," + "SHN End Date" + "," + "SHN Facility Name" + "\n";
                File.WriteAllText("SHNStatusReporting.csv", heading); //writing to file
                foreach (Person p in pList)
                {
                    foreach (TravelEntry te in p.TravelEntryList)
                    {
                        if (te.ShnStay != null)
                        {
                            //SHNFacility sf = SearchFacility(sList, te.ShnStay.FacilityName);
                            string data = p.Name + "," + Convert.ToString(te.ShnEndDate) + "," + te.ShnStay.FacilityName + "\n";
                            File.AppendAllText("SHNStatusReporting.csv", data); //appending data to file
                            checkDone = true;
                        }
                    }
                }
                if (checkDone == true)
                {
                    Console.WriteLine("Report generated successfully\n");
                }
                else
                {
                    Console.WriteLine("There is no person to generate the report\n");
                }
            }
            catch (FormatException)
            {
                Console.WriteLine("Please enter a valid value!\n");
            }
            catch (Exception)
            {
                Console.WriteLine("Please enter a valid value!\n");
            }
        }
        //InfectedPerson method to change the infected status of the people that visited the Business in a given date
        static void InfectedPerson(List<Person> pList, List<BusinessLocation> bList)
        {
            try
            {
                Console.Write("Enter Business location that is suspected infection: "); //prompt user for input
                string bName = Console.ReadLine();
                BusinessLocation b = SearchBusinessLocation(bList, bName); //calling SearchBusinessLocation method
                if (b != null) //check for BusinessLocation object is not null
                {
                    Console.Write("Enter start date of suspected infection (dd/MM/yyyy): "); //prompt user for input
                    DateTime start = Convert.ToDateTime(Console.ReadLine());
                    Console.Write("Enter end date of suspected infection (dd/MM/yyyy): "); //prompt user for input
                    DateTime end = Convert.ToDateTime(Console.ReadLine());

                    foreach (Person p in pList)
                    {
                        foreach (SafeEntry se in p.SafeEntryList)
                        {
                            if (se.Location.BusinessName == b.BusinessName && se.CheckIn.Date >= start.Date && se.CheckOut.Date <= end.Date)
                            {
                                p.IsInfected = true;
                            }
                        }
                    }
                    Console.WriteLine("The infected person(s) has been recorded\n");
                }
                else
                {
                    Console.WriteLine("No such business exist!\n");
                }
            }
            catch (FormatException)
            {
                Console.WriteLine("Please enter a valid value!\n");
            }
            catch (Exception)
            {
                Console.WriteLine("Please enter a valid value!\n");
            }
        }
        //GenerateInfectedPersonReport method to generate a report of those person who is being infected
        static void GenerateInfectedPersonReport(List<Person> pList)
        {
            try
            {
                bool checkDone = false;
                Console.Write("Please enter a start date for the report (dd/MM/yyyy): "); //prompt user for input
                DateTime startDate = Convert.ToDateTime(Console.ReadLine());
                string heading = "Name" + "," + "Location" + "," + "Check In" + "," + "Check Out" + "\n";
                File.WriteAllText("InfectedPersonReport.csv", heading); //write to file
                foreach (Person p in pList)
                {
                    foreach (SafeEntry se in p.SafeEntryList)
                    {
                        if (startDate <= se.CheckIn && p.IsInfected == true)
                        {
                            string data = p.Name + "," + se.Location.BusinessName + "," + se.CheckIn + "," + se.CheckOut + "\n";
                            File.AppendAllText("InfectedPersonReport.csv", data); //appending data to file
                            checkDone = true;
                        }
                    }
                }
                if (checkDone == true)
                {
                    Console.WriteLine("Infected Person Report generated successfully\n");
                }
                else
                {
                    Console.WriteLine("There is no person to generate the report\n");
                }
            }
            catch (FormatException)
            {
                Console.WriteLine("Please enter a valid value!\n");
            }
            catch (Exception)
            {
                Console.WriteLine("Please enter a valid value!\n");
            }
        }
    }//end of program
}//end of namespace


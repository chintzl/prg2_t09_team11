﻿//============================================================
// Student Number : S10208283, S10208484
// Student Name : To Chin Liang, Irene Phang Xin Ping
// Module Group : T09
//============================================================
using System;
using System.Collections.Generic;
using System.Text;

namespace COVID_19_Monitoring_System
{
    class SafeEntry 
    {
        //Attributes
        public DateTime CheckIn { get; set; }
        public DateTime CheckOut { get; set; }
        public BusinessLocation Location { get; set; }
        //End of Attributes

        //Constructors
        public SafeEntry() { }
        public SafeEntry(DateTime i,BusinessLocation l)
        {
            CheckIn = i;
            Location = l;
        }
        //End of Constructors

        //Methods
        public void PerformCheckOut()
        {
            CheckOut = DateTime.Now;
        }

        public override string ToString()
        {
            return "Check In: " + CheckIn + "\nCheck Out: " + CheckOut + "\nLocation: " + Location;
        }
        //End of Methods
    }//End of SafeEntry class
}//End of namespace

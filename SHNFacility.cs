﻿//============================================================
// Student Number : S10208283, S10208484
// Student Name : To Chin Liang, Irene Phang Xin Ping
// Module Group : T09
//============================================================
using System;
using System.Collections.Generic;
using System.Text;

namespace COVID_19_Monitoring_System
{
    class SHNFacility
    {
        //Attributes
        public string FacilityName { get; set; }
        public int FacilityCapacity { get; set; }
        public int FacilityVacancy { get; set; }
        public double DistFromAirCheckPoint { get; set; }
        public double DistFromSeaCheckPoint { get; set; }
        public double DistFromLandCheckPoint { get; set; }
        //End of Attributes

        //Constructors
        public SHNFacility() { }
        public SHNFacility(string fn, int fc, double da, double ds, double dl)
        {
            FacilityName = fn;
            FacilityCapacity = fc;
            DistFromAirCheckPoint = da;
            DistFromSeaCheckPoint = ds;
            DistFromLandCheckPoint = dl;
        }
        //End of Constructors

        //Methods
        public double CalculateTravelCost(string em, DateTime ed)
        {
            double baseFare = 0.0;
            if (em == "Land") //calculate basefare for Land
            {
                baseFare += 50 + (DistFromLandCheckPoint * 0.22);
            }
            else if (em == "Air") //calculate basefare for Air
            {
                baseFare += 50 + (DistFromAirCheckPoint * 0.22);
            }
            else if (em == "Sea") //calculate basefare for Sea
            {
                baseFare += 50 + (DistFromSeaCheckPoint * 0.22);
            }
            DateTime startMidnight = Convert.ToDateTime("12:00 AM");
            DateTime endMidnight = Convert.ToDateTime("5:59 AM");

            if (startMidnight.TimeOfDay < ed.TimeOfDay)
            {
                if(endMidnight.TimeOfDay > ed.TimeOfDay)
                {
                    baseFare = baseFare * 1.50;
                }
            }
            else
            {
                baseFare = baseFare * 1.25;
            }
            return baseFare;
        }
        public bool IsAvailable()
        {
            if (FacilityVacancy > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public override string ToString()
        {
            return "Facility Name: " + FacilityName + "\nFacility Capacity: " + FacilityCapacity + "\nDistance from Air Checkpoint: " + DistFromAirCheckPoint + "\nDistance from Sea Checkpoint: " + DistFromSeaCheckPoint + "\nDistance from Land Checkpoint: " + DistFromLandCheckPoint;
        }
        //End of Methods
    }//End of SHNFacility class
}//End of namespace

﻿//============================================================
// Student Number : S10208283, S10208484
// Student Name : To Chin Liang, Irene Phang Xin Ping
// Module Group : T09
//============================================================
using System;
using System.Collections.Generic;
using System.Text;

namespace COVID_19_Monitoring_System
{
    class Visitor:Person
    {
        //Attributes
        public string PassportNo { get; set; }
        public string Nationality { get; set; }
        //End of Attributes

        //Constructors
        public Visitor(string n, string p, string na) : base(n)
        {
            PassportNo = p;
            Nationality = na;
        }
        //End of Constructors

        //Methods
        public override double CalculateSHNCharges()
        {
            double cost = 0.0;
            cost += 200; //Swab Test
            foreach (TravelEntry te in TravelEntryList)
            {
                if (te.LastCountryOfEmbarktion == "New Zealand" || te.LastCountryOfEmbarktion == "Vietnam")
                {
                    cost += 80; //Transportation
                }
                else if (te.LastCountryOfEmbarktion == "Macao SAR")
                {
                    cost += 80; //Transporation
                }
                else
                {
                    cost += te.ShnStay.CalculateTravelCost(te.EntryMode, te.EntryDate); //Transportation
                    cost += 2000; //SDF Charge
                }
            }
            return cost;
        }
        public override string ToString()
        {
            return base.ToString() + "PassportNo: " + PassportNo + "\nNationality: " + Nationality;
        }
        //End of Methods
    }//End of Visitor class
}//End of namespace

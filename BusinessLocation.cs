﻿//============================================================
// Student Number : S10208283, S10208484
// Student Name : To Chin Liang, Irene Phang Xin Ping
// Module Group : T09
//============================================================
using System;
using System.Collections.Generic;
using System.Text;

namespace COVID_19_Monitoring_System
{
    class BusinessLocation
    {
        //Attributes
        public string BusinessName { get; set; }
        public string BranchCode { get; set; }
        public int MaximumCapacity { get; set; }
        public int VisitorsNow { get; set; }
        //End of Attributes

        //Constructors
        public BusinessLocation() { }
        public BusinessLocation(string bn, string bc, int mc)
        {
            BusinessName = bn;
            BranchCode = bc;
            MaximumCapacity = mc;
        }
        //End of Constructors

        //Methods
        public bool IsFull()
        {
            if (VisitorsNow >= MaximumCapacity)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public override string ToString()
        {
            return "Business Name: " + BusinessName + "\nBranch Code: " + BranchCode + "\nMaximum Capacity: " + MaximumCapacity + "\n";
        }
        //End of Methods
    }//End of BusinessLocation class
}//End of namespace

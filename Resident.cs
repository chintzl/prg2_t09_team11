﻿//============================================================
// Student Number : S10208283, S10208484
// Student Name : To Chin Liang, Irene Phang Xin Ping
// Module Group : T09
//============================================================
using System;
using System.Collections.Generic;
using System.Text;

namespace COVID_19_Monitoring_System
{
    class Resident:Person
    {
        //Attributes
        public string Address { get; set; }
        public DateTime LastLeftCountry { get; set; }
        public TraceTogetherToken Token { get; set; }
        //End of Attributes

        //Constructors
        public Resident(string n, string a, DateTime l) : base(n)
        {
            Address = a;
            LastLeftCountry = l;
            Token = new TraceTogetherToken();
        }
        //End of Constructors

        //Methods
        public override double CalculateSHNCharges()
        {
            double cost = 0.0;
            cost += 200; //Swab Test
            foreach (TravelEntry te in TravelEntryList)
            {
                if (te.LastCountryOfEmbarktion == "New Zealand" || te.LastCountryOfEmbarktion == "Vietnam")
                {
                    cost += 0; //Transportation
                }
                else if (te.LastCountryOfEmbarktion == "Macao SAR")
                {
                    cost += 20; //Transporation
                }
                else
                {
                    cost += 20; //Transportation
                    cost += 1000; //SDF Charge
                }
            }
            return cost;
        }
        public override string ToString()
        {
            return base.ToString() + "Address: " + Address + "\nLast Left Country: " + LastLeftCountry;
        }
        //End of Methods
    }//End of Resident class
}//End of namespace

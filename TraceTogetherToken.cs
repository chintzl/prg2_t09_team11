﻿//============================================================
// Student Number : S10208283, S10208484
// Student Name : To Chin Liang, Irene Phang Xin Ping
// Module Group : T09
//============================================================
using System;
using System.Collections.Generic;
using System.Text;

namespace COVID_19_Monitoring_System
{
    class TraceTogetherToken
    {
        //Attributes
        public string SerialNo { get; set; }
        public string CollectionLocation { get; set; }
        public DateTime ExpiryDate { get; set; }
        //End of Attributes

        //Constructors
        public TraceTogetherToken() { }
        public TraceTogetherToken(string s, string c, DateTime e)
        {
            SerialNo = s;
            CollectionLocation = c;
            ExpiryDate = e;
        }
        //End of Constructor

        //Methods
        public bool IsEligibleForReplacement()
        {
            TimeSpan diff = ExpiryDate - DateTime.Now;
            if(diff.Days <= 30)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public TraceTogetherToken ReplaceToken(string s, string c)
        {
            return new TraceTogetherToken(s, c,DateTime.Now);
        }
        public override string ToString()
        {
            return "Serial No: " + SerialNo + "\nCollection Location: " + CollectionLocation + "\nExpiry Date: " + ExpiryDate;
        }
        //End of Methods
    }//End of TraceTogetherToken class
}//End of namespace
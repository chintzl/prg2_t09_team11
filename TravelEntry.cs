﻿//============================================================
// Student Number : S10208283, S10208484
// Student Name : To Chin Liang, Irene Phang Xin Ping
// Module Group : T09
//============================================================
using System;
using System.Collections.Generic;
using System.Text;

namespace COVID_19_Monitoring_System
{
    class TravelEntry
    {
        //Attributes
        public string LastCountryOfEmbarktion { get; set; }
        public string EntryMode { get; set; }
        public DateTime EntryDate { get; set; }
        public DateTime ShnEndDate { get; set; }
        public SHNFacility ShnStay { get; set; }
        public bool IsPaid { get; set; }
        //End of Attributes

        //Constructors
        public TravelEntry() { }    
        public TravelEntry(string l, string em, DateTime ed)
        {
            LastCountryOfEmbarktion = l;
            EntryMode = em;
            EntryDate = ed;
        }
        //End of Constructors

        //Methods
        public void AssignSHNFacility(SHNFacility s)
        {
            ShnStay = s;
            s.FacilityVacancy -= 1;
        }

        public void CalculateSHNDuration()
        {
            DateTime start = EntryDate;
            DateTime end;

            if (LastCountryOfEmbarktion == "New Zealand" || LastCountryOfEmbarktion == "Vietnam")
            {
                end = start;
            }

            else if (LastCountryOfEmbarktion == "Macao SAR")
            {
                end = start.AddDays(7);
            }
            else
            {
                end = start.AddDays(14);
            }
            ShnEndDate = end;
        }
        public override string ToString()
        {
            return "Last Country of Embarkation: " + LastCountryOfEmbarktion + "\nEntry Mode: " + EntryMode + "\nEntry Date: " + EntryDate + "\n";
        }
        //End of Methods
    }//End of TravelEntry class
}//End of namespace
